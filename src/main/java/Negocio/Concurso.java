/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dto.Estudiante;
import java.util.TreeSet;

/**
 *
 * @author ACER
 */
public class Concurso {
      TreeSet<Estudiante> estudiantes=new TreeSet();

    public Concurso() {
    }
      
     public boolean insertarEstudiante(String nombre,String correo,String contraseña){
      Estudiante x=new Estudiante ();        
      if(validarCorreo(correo)==true&&validarContraseña(contraseña)==true){
      x.setNombre(nombre);
      x.setContraseña(contraseña);
      x.setEmail(correo);
      } return this.estudiantes.add(x);
     }
     private boolean validarCorreo(String correo){
         String dominio ="ufps.edu.co";
         String[] p = correo.split("@");
         String p2 = p[1];
         if(p2.equalsIgnoreCase(dominio)){
         return true;
         }
         else{
         return false;
         }
     }
     
     private boolean validarContraseña(String contraseña){
         int num=contraseña.length();
          if(num==6){
          return true;
          }else{return false;}         
     }
     public Estudiante buscarEst(String correo){
         Estudiante x=new Estudiante();
         x.setEmail(correo);
         if(this.estudiantes.contains(x)){
         return this.estudiantes.floor(x);
         }
          return null;
     }
     
     public TreeSet<Estudiante> getEstudiantes() {
        return estudiantes;
    }
    
     
    /* public static void main (String []args){
       Concurso c=new Concurso ();
       
         System.out.println(c.validarCorreo("ana@gmail.com"));
     }*/
   
}
