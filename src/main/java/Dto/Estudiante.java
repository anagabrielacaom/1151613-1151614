/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

/**
 *
 * @author ACER
 */
public class Estudiante implements Comparable {
    String nombre;
    String email;
    String contraseña;

    public Estudiante() {
    }

    public Estudiante(String nombre, String email, String contraseña) {
        this.nombre = nombre;
        this.email = email;
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    @Override
    public String toString() {
        return "Estudiante" + "nombre=" + nombre + ", email=" + email + ", contraseña=" + contraseña;
    }


    @Override
    public int compareTo(Object o) {
       Estudiante x=(Estudiante)o;     
     return ((int)(getEmail().hashCode())-x.getEmail().hashCode());
    }  
}
